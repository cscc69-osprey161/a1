/*
 * Process-related syscalls.
 * New for ASST1.
 */

#include <types.h>
#include <kern/errno.h>
#include <kern/wait.h>
#include <lib.h>
#include <thread.h>
#include <current.h>
#include <pid.h>
#include <machine/trapframe.h>
#include <machine/vm.h>
#include <syscall.h>
#include <signal.h>

/*
 * sys_fork
 * 
 * create a new process, which begins executing in md_forkentry().
 */


int
sys_fork(struct trapframe *tf, pid_t *retval)
{
	struct trapframe *ntf; /* new trapframe, copy of tf */
	int result;

	/*
	 * Copy the trapframe to the heap, because we might return to
	 * userlevel and make another syscall (changing the trapframe)
	 * before the child runs. The child will free the copy.
	 */

	ntf = kmalloc(sizeof(struct trapframe));
	if (ntf==NULL) {
		return ENOMEM;
	}
	*ntf = *tf; /* copy the trapframe */

	result = thread_fork(curthread->t_name, enter_forked_process, 
			     ntf, 0, retval);
	if (result) {
		kfree(ntf);
		return result;
	}

	return 0;
}

/*
* Get PID for current thread.
*/
int
sys_getpid(pid_t *mypid){
	*mypid = curthread->t_pid;
	return 0;
}

/*
* Wait for the process with given pid to exit. Returns immediately 
* if specified process has already exited. 
*
* Note that: 
* - The calling thread can only execute waitpid on child threads. 
* - A thread is considered 'does not exist' once it exited, and has
*   no parent.
*
* Returns the PID of the process. If status is non-null, the exit 
* status of the target thread is returned in there.
*/
int 
sys_waitpid(pid_t pid, int *status, int options, pid_t *exitpid){
	int result; // Stores result from function calls.

	// Only supports WNOHANG currently.
	if (options != 0 && options != WNOHANG){
		return EINVAL;
	}
	// Errors if pid does not exist, or current thread isn't parent of pid.
	result = pid_is_parent_of(pid);
	if (result){
		//kprintf(">>ERR Thread %d waiting for thread %d (%d)\n", curthread->t_pid, pid, result);
		return result;
	}

	if (status == NULL || status <= (int *)0x40000000 || status >= (int *)0x80000000){
		//kprintf(">>ERR Thread %d waiting for thread %d (%d)\n", curthread->t_pid, pid, EFAULT);	
		return EFAULT;
	}

	// Wait for target pid using pid_join.
	//kprintf(">>W Thread %d waiting for thread %d\n", curthread->t_pid, pid);
	result = pid_join(pid, status, options);
	//kprintf(">>E Thread %d finished waiting for thread %d w/ result %d\n", curthread->t_pid, pid, result);

	// pid_join returning negative result => error.
	if (result < 0){
		return -result; // Flip the error code back to positive.
	}
	// pid_join returning 0 => pid still running (WNOHANG was set)
	*exitpid = result;
	return 0;
}

/*
 * "Kills" the thread indicated by pid by setting the kill_flag 
 * The thread pid will check kill_flag between returning to user_space
 */
int
sys_kill(pid_t pid, int sig, int *result){

	int res;

	// Check for invalid signal number
	if (sig < 0 || sig > 31) {
		*result = -1;
		return EINVAL;
	}

	// Check for unimplemented Signals
	switch (sig){
		case 0:
		case SIGHUP:
		case SIGINT:
		case SIGKILL:
		case SIGTERM:
		case SIGSTOP:
		case SIGCONT:
		case SIGWINCH:
		case SIGINFO:
		break;
		default:
			*result = -1;
			return EUNIMP;	
		break;
	}


	// Check for non-existent pid
	res = pid_set_kill_flag(pid, sig);
	if (res) {
		*result = -1;
		return res;
	}

	// Wake the target thread if a SIGCONT signal was sent.
	if (sig == SIGCONT){
		res = pid_signal_continue(pid);
		if (res) {
			*result = -1;
			return res;
		}
	}

	*result = 0;
	return 0;
}
 
