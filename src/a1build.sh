#/bin/bash

set -e

mkdir -p $HOME/cscc69/root

./configure

cd kern/conf
./config ASST1

cd ../compile/ASST1
bmake depend
bmake
bmake install

cd ../../..

if [ "$1" = "-a" ]; then
bmake
bmake install
fi

cp ./sys161.conf $HOME/cscc69/root/sys161.conf

#if [ "$1" = "-r" ]; then
#cd $HOME/cscc69/root
#sys161 kernel
#fi
